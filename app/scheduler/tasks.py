import dramatiq

from parsing.parsers import Parser


@dramatiq.actor
def parse_task(parser: Parser, *args, **kwargs):
    parser.parse(*args, **kwargs)
