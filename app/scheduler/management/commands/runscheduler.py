from apscheduler.schedulers.blocking import BlockingScheduler
from constance import config
from django.conf import settings
from django.core.management import BaseCommand
from loguru import logger

from scheduler.jobs import (
    check_work_simsale,
    clean_broker,
    clean_job_executions,
    clear_active_activations,
    push_sms_job,
    reset_delivery_attempts,
    update_availability,
    update_sms_job,
)


class Command(BaseCommand):
    help = "Init and run scheduler"

    def handle(self, *args, **options):
        scheduler = BlockingScheduler()
        # TODO: autodiscover jobs and params for them.
        scheduler.add_job(
            update_sms_job,
            trigger="interval",
            seconds=config.UPDATE_SMS_INT,
            id="update_sms_job",
            max_instances=1,
            replace_existing=True,
        )
        scheduler.add_job(
            push_sms_job,
            trigger="interval",
            seconds=config.PUSH_SMS_INT,
            id="push_sms_job",
            max_instances=1,
            replace_existing=True,
        )
        scheduler.add_job(
            update_availability,
            trigger="interval",
            seconds=config.UPDATE_AVAILABILITY_INT,
            id="update_availability",
            max_instances=1,
            replace_existing=True,
        )
        scheduler.add_job(
            clean_broker,
            trigger="interval",
            seconds=settings.DRAMATIQ_CLEAN_MSG_INT,
            id="clean_broker",
            max_instances=1,
            replace_existing=True,
        )
        scheduler.add_job(
            clean_job_executions,
            trigger="interval",
            seconds=settings.JOBS_CLEAN_EXECUTION_INT,
            id="clean_job_executions",
            max_instances=1,
            replace_existing=True,
        )
        scheduler.add_job(
            check_work_simsale,
            trigger="interval",
            seconds=config.SEND_NUMBER_V2_INTERVAL,
            id="check_work_simsale",
            max_instances=1,
            replace_existing=False,
        )
        scheduler.add_job(
            clear_active_activations,
            trigger="cron",
            hour="0",
            id="clear_active_activations",
            max_instances=1,
            replace_existing=False,
        )
        scheduler.add_job(
            reset_delivery_attempts,
            trigger="cron",
            hour="2",
            id="reset_delivery_attempts",
            max_instances=1,
            replace_existing=True,
        )

        logger.info("Starting scheduler...")
        scheduler.start()
