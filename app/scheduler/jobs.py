import dramatiq
from django.conf import settings
from django_apscheduler import util
from django_apscheduler.models import DjangoJobExecution

from common.enums import MarketPlace
from items.models import Category
from parsing.parsers import LazadaJSONParser, ShopeeJSONParser
from scheduler import tasks


@util.close_old_connections
def clean_job_executions():
    """
    This job deletes APScheduler job execution entries older than `max_age` from the database.
    It helps to prevent the database from filling up with old historical records that are no
    longer useful.
    """
    DjangoJobExecution.objects.delete_old_job_executions(
        settings.JOBS_EXECUTION_MAX_AGE
    )


@util.close_old_connections
def run_parsing():
    for category in Category.objects.filter(is_active=True):
        parser_class = LazadaJSONParser if category.market_place == MarketPlace.LZ else ShopeeJSONParser
        parser = parser_class(category_name=category.name)
        tasks.parse_task.send(parser=parser)
