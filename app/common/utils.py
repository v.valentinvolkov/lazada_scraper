from collections.abc import Iterable, Mapping
from types import MappingProxyType
from urllib.parse import urlunsplit, urlencode, urlsplit

from django.utils.html import format_html


class URL:

    def __init__(
            self,
            url: str | None = None,
            *,
            netlog: str | None = None,
            path: Iterable[str] = ("",),
            query: Mapping[str, str] = MappingProxyType({}),
            scheme: str = "https"
    ):
        if url:
            parse = urlsplit(url)
            self.scheme = parse.scheme
            self.netlog = parse.netloc
            self.path = parse.path
            self.query = parse.query
        elif scheme and netlog:
            self.scheme = scheme
            self.netlog = netlog
            self.path = path
            self.query = query
        else:
            raise Exception('Set "url" or netlog, path, query and scheme')

    def change_query(self, params: Mapping[str, str]):
        self.query = {**self.query, **params}

    def __str__(self):
        return urlunsplit((
            self.scheme,
            self.netlog,
            "/".join(self.path),
            urlencode(self.query),
            ""
        ))


def format_link(href: str, text: str) -> str:
    return f'<a href="{href}">{text}</>'
