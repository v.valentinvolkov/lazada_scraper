from django.db import models


class MarketPlace(models.TextChoices):
    LZ = "LZ", "Lazada"
    SH = "SH", "Shopee"
