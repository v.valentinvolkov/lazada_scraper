from typing import Optional

from selenium import webdriver
from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement


class ExtendedChrome(webdriver.Chrome):
    def find_element(
            self, by=By.ID, value: Optional[str] = None, time_to_wait: int = 0, iterations: int = 1
    ) -> WebElement:
        for tries in range(iterations):
            try:
                return super().find_element(by, value)
            except NoSuchElementException as e:
                if tries >= iterations - 1:
                    raise e
                self.implicitly_wait(time_to_wait)
