from typing import Type

from django.conf import settings
from django.db import models

from common.enums import MarketPlace
from parsing.parsers import Parser, LazadaJSONParser, ShopeeJSONParser


class Item(models.Model):
    item_id = models.CharField("Outer ID", max_length=255)
    market_place = models.CharField("Площадка", choices=MarketPlace.choices, max_length=255)
    name = models.CharField("Название", max_length=1000)
    img = models.URLField("Обложка", max_length=1000)
    categories = models.ManyToManyField(
        "Category",
        verbose_name="Категории",
        related_name="items",
        related_query_name="item"
    )
    url = models.URLField("URL", max_length=1000)


class ItemStat(models.Model):
    price = models.PositiveIntegerField("Цена", null=True)
    discount = models.PositiveIntegerField("Скидка", null=True)
    sales = models.PositiveIntegerField("Проданно", null=True)
    rating_score = models.PositiveIntegerField("Рейтинг", null=True)
    reviews = models.PositiveIntegerField("Оценки", null=True)

    created_dt = models.DateTimeField("Время парсинга", auto_now_add=True)

    item = models.ForeignKey("Item", related_name="stats", related_query_name="stat", on_delete=models.CASCADE)

    parsing = models.ForeignKey(
        "parsing.Parsing", related_name="+", related_query_name="+", null=True, on_delete=models.SET_NULL
    )

    class Meta:
        ordering = ["-created_dt"]


class Category(models.Model):
    external_id = models.CharField("Внешний идентификатор", max_length=1000, primary_key=True)
    name = models.CharField("Название категории", max_length=1000, unique=True)
    market_place = models.CharField("Площадка", choices=MarketPlace.choices, max_length=255)
    is_active = models.BooleanField("Парсить", default=True)

    parent_category = models.ForeignKey(
        "Category",
        on_delete=models.SET_NULL,
        related_name="sub_categories",
        related_query_name="sub_category",
        null=True
    )

    @property
    def has_sub_categories(self) -> bool:
        return bool(self.parent_category)


class LazadaCategory(Category):
    @property
    def url(self):
        return f"{settings.LAZADA_BASE_URL}/{self.external_id}?ajax=true"

    class Meta:
        proxy = True


class ShopeeCategory(Category):
    @property
    def url(self):
        return f"{settings.SHOPEE_BASE_URL}/api/v4/recommend/recommend?bundle=category_landing_page&catid={self.external_id}"

    class Meta:
        proxy = True
