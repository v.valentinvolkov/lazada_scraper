from django.contrib import admin
from django.db.models import OuterRef, Subquery
from django.urls import reverse
from django.utils.html import format_html

from items.models import Item, ItemStat, Category


class ItemStatAdmin(admin.TabularInline):
    model = ItemStat
    extra = 0
    fields = ("created_dt", "sales", "reviews", "rating_score_display", "price_display", "display_discount")
    readonly_fields = ("created_dt", "sales", "reviews", "rating_score_display", "price_display", "display_discount")
    list_per_page = 20

    @admin.display(description="Цена")
    def price_display(self, obj: ItemStat):
        return f"{obj.price} ฿"

    @admin.display(description="Скидка")
    def display_discount(self, obj: ItemStat):
        return f"{obj.discount}%"

    @admin.display(description="Рейтинг")
    def rating_score_display(self, obj: ItemStat):
        return f"{float(obj.rating_score) / 10} ★"

    def has_change_permission(self, request, obj=None):
        return False


class ItemRatingScoreFilter(admin.SimpleListFilter):
    title = "Оценки"
    parameter_name = "rating_score"

    def lookups(self, request, model_admin):
        return (
            ("10", "★"),
            ("20", "★★"),
            ("30", "★★★"),
            ("40", "★★★★"),
            ("50", "★★★★★"),
        )

    def queryset(self, request, queryset):
        if self.value():
            first_stat = (
                ItemStat.objects
                .filter(item=OuterRef("pk"))
                .order_by("-created_dt")
            )
            return queryset.annotate(
                first_stat_rating_score=Subquery(first_stat.values("rating_score")[:1])
            ).filter(
                first_stat_rating_score__gte=self.value()
            )

        return queryset


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ["img_list_preview", "display_url", "sales", "reviews", "price"]
    fields = [
        "item_id", "name", "img_preview", "display_url",
        "price", "discount", "sales",  "rating_score", "reviews", "category_list"
    ]
    readonly_fields = ("display_name", "img_preview", "price", "rating_score", "reviews", "sales", "discount",
                       "display_url", "category_list")
    list_filter = (ItemRatingScoreFilter,)
    list_per_page = 20
    inlines = (ItemStatAdmin,)
    search_fields = ("item_id",)

    @admin.display(description="Обложка (превью)")
    def img_preview(self, obj: Item):
        return format_html(f'<a href="{obj.img}" ><img src="{obj.img}" style="max-width:200px; max-height:200px"/></a>')

    @admin.display
    def img_list_preview(self, obj: Item):
        return format_html(f'<img src="{obj.img}" style="max-width:70px; max-height:70px"/>')

    @admin.display(description="Ссылка на товар")
    def display_url(self, obj: Item):
        return format_html(f'<a href="{obj.url}">{obj.item_id}</a>')

    @admin.display
    def display_name(self, obj: Item):
        return f"{obj.name[:40]}..."

    @admin.display(description="Цена")
    def price(self, obj: Item):
        return f"{obj.stats.first().price} ฿"

    @admin.display(description="Рейтинг")
    def rating_score(self, obj: Item):
        return f"{float(obj.stats.first().rating_score)/10} ★"

    @admin.display(description="Оценки")
    def reviews(self, obj: Item):
        return obj.stats.first().reviews

    @admin.display(description="Продано")
    def sales(self, obj: Item):
        return obj.stats.first().sales

    @admin.display(description="Скидка")
    def discount(self, obj: Item):
        return f"{obj.stats.first().discount}%"

    # TODO: ссылка не работает как надо!
    @admin.display(description="Категории")
    def category_list(self, obj: Item):
        li_tags = ""
        for cat in obj.categories.all():
            href = reverse("admin:items_category_change", args=(cat.pk,)),
            li_tags += f'<li><a href="#">{cat.name}</a></li>'

        return format_html(f'<ul style="margin: 0; padding: 0;">{li_tags}</ul>')

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ["name", "external_id", "market_place"]
    fields = ["name", "external_id", "market_place"]
