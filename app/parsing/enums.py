from django.db import models


class ParsingStatus(models.TextChoices):
    PLANNED = "PLANNED", "Запланировано"
    RUNNING = "RUNNING", "В процессе"
    COMPLETED = "COMPLETED", "Завершено"
    FAILED = "FAILED", "Прервано"
