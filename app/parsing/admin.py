from django.contrib import admin
from django_object_actions import action, DjangoObjectActions

from parsing.models import Parsing


@admin.register(Parsing)
class ParsingAdmin(DjangoObjectActions, admin.ModelAdmin):
    list_display = ["id", "start_dt", "end_dt", "status"]
    fields = [
        "id",
        ("start_dt", "end_dt"),
        "expected_items", "captcha_count", "errors"
    ]
    list_per_page = 20

    def has_change_permission(self, request, obj=None):
        return False
