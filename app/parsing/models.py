from django.db import models
from django.utils import timezone

from parsing.enums import ParsingStatus


class Parsing(models.Model):
    start_dt = models.DateTimeField("Время начала", null=True)
    end_dt = models.DateTimeField("Время окончания", null=True)
    status = models.CharField("Статус", choices=ParsingStatus.choices, null=True, default=None)

    captcha_count = models.PositiveSmallIntegerField("Кол. капчей", default=0)
    errors = models.PositiveSmallIntegerField("Кол. ошибок", default=0)

    category = models.ForeignKey(
        "items.Category",
        on_delete=models.CASCADE,
        related_name="+",
        related_query_name="+"
    )

    def __str__(self):
        return f'{self.pk} "{self.category.name}"'

    def set_status(self, status: ParsingStatus, save: bool = False):
        self.status = status
        if save:
            self.save()

    def run(self):
        self.start_dt = timezone.now()
        self.set_status(ParsingStatus.RUNNING)
        self.save()

    def stop(self, error: bool = False):
        self.end_dt = timezone.now()
        self.set_status(ParsingStatus.FAILED if error else ParsingStatus.COMPLETED)
        self.save()
