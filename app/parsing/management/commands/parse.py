from datetime import datetime
from pathlib import Path
from typing import Optional

from django.core.management import BaseCommand

from parsing.parsers import LazadaJSONParser


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--category", type=str, default=None, help="LZ category name.")
        parser.add_argument("--items", type=int, default=None, help="Total items to parse.")
        parser.add_argument("--captcha-wait", type=int, default=60, help="Interval to make a captcha.")
        parser.add_argument("--page-param", type=str, default=None, help="GET param to paginate.")

    def handle(self, *args, **options):
        parser = LazadaJSONParser(options["category"])
        parser.parse(item_limit=options.get("items"))
