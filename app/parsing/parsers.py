import json
import re
import time
from collections.abc import Generator, Mapping
from types import MappingProxyType

from django.conf import settings
from django.utils import timezone
from loguru import logger
from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver

import undetected_chromedriver as uc
from common.enums import MarketPlace
from common.utils import URL
from items.models import ItemStat, Item, Category
from parsing.models import Parsing
from parsing.formate import LazadaFormate, ShopeeFormate

type Params = Mapping


class Parser:
    market_place: MarketPlace
    default_params: Mapping = MappingProxyType({})

    def __init__(
            self,
            category: Category,
            params: Params = MappingProxyType({})
    ):
        self._parsing: Parsing | None = None
        self._driver: WebDriver | None = None
        self._category = category

        self.category_name = category.name
        self.params = {**self.default_params, **params}

        # TODO: как то иначе биндить логеры!?
        logger.add(
            f"{settings.PARSING_LOG_ROOT}/{timezone.now().date().strftime("%m-%d-%y")}/{self.market_place}/{self.category_name}.log",
            format="{time} {level} {message}",
            filter=lambda record: record["extra"]["parser"] == self.__class__.__name__,
            level="INFO"
        )
        self.logger = logger.bind(parser=self.__class__.__name__)

    def page_url_gen(self) -> Generator:
        ...

    @property
    def category(self) -> Category:
        return self._category

    @property
    def parsing(self) -> Parsing:
        if not self._parsing:
            self._parsing = Parsing.objects.create(category=self.category)
        return self._parsing

    @property
    def driver(self) -> WebDriver:
        if not self._driver:
            self._driver = uc.Chrome(headless=False, use_subprocess=False)
        return self._driver

    def pre_parse(self):
        pass

    def page_parse(self, url: str, captcha_wait: int, *args, **kwargs) -> Generator[Item]:
        """Generator to get each item from page."""
        ...

    def parse(self, item_limit: int | None = None, error_limit: int | None = 5, captcha_wait: int = 60):
        """
        Run parse.

        Args:
            item_limit: soft limit of total items to parse (depend on page size);
            error_limit: limit of any errors during run;
            captcha_wait: total wait time to handle capcha.

        """
        self.start_log(item_limit, error_limit, captcha_wait)
        self.parsing.run()

        self.pre_parse()

        total_items = 0
        for url in self.page_url_gen():
            try:
                self.logger.info(f"Parse {url}")

                for _ in self.page_parse(url, captcha_wait=captcha_wait):
                    total_items += 1
                    if item_limit and item_limit <= total_items:
                        break  # Break current page parsing
                else:
                    continue  # Continue "paginate"
                break  # Break "paginate"

            except Exception as e:
                self.logger.exception(e)
                self.parsing.errors = self.parsing.errors + 1
                if self.parsing.errors >= error_limit:
                    self.logger.error("Too many errors!")
                    raise e

        self.parsing.stop()
        self.driver.quit()

    def start_log(self, item_limit, error_limit, captcha_wait):
        self.logger.info(
            f"\n=====================================\n"
            f"Item limit: {item_limit}\n"
            f"Error limit: {error_limit}\n"
            f"Captcha wait: {captcha_wait}\n"
            f"====================================="
        )


class ShopeeJSONParser(Parser):
    market_place = MarketPlace.SH
    default_params = {"bundle": "category_landing_page"}

    _login_url = URL(netlog=settings.SHOPEE_BASE_URL, path=("buyer", "login"))

    def page_url_gen(self) -> Generator[str]:
        url = URL(
            netlog=settings.SHOPEE_BASE_URL,
            path=("api", "v4", "recommend", "recommend"),
            query={**self.params, "catid": self.category.external_id, "limit": 1000, "offset": 0}
        )
        yield str(url)

    def pre_parse(self):
        self.driver.get(str(self._login_url))
        self.driver.implicitly_wait(5)

        # Choice of language
        self.driver.implicitly_wait(5)
        self.driver.find_element(By.XPATH, '//*[contains(text(),"English")]').click()

        # Login
        form = self.driver.find_element(By.TAG_NAME, "form")
        form.find_element(By.NAME, "loginKey").send_keys("ynchinhau@gmail.com")
        form.find_element(By.NAME, "password").send_keys("qwe123QWE!@#")
        form.find_elements(By.TAG_NAME, "button")[1].click()
        time.sleep(10)

    def page_parse(self, url: str, captcha_wait: int, *args, **kwargs) -> Generator[Item]:
        js = f'return await fetch("{url}").then((response) => response.json())'
        content = self.driver.execute_script(js)
        yield from self.collect_items_gen(content)

    def collect_items_gen(self, content: dict | list) -> Generator[Item]:
        list_items = content["data"]["sections"][0]["data"]["item"]
        for item_data in list_items:
            url_item_name = re.sub(r'\s|%|&|<|>|"|{|}|\\|^|`|\|', "-", item_data["name"])
            url = URL(
                netlog=settings.SHOPEE_BASE_URL,
                path=(f"{url_item_name}-i.{item_data["shopid"]}.{item_data["itemid"]}",)
            )

            item, created = Item.objects.get_or_create(
                item_id=item_data["itemid"],
                defaults={
                    "name": item_data["name"],
                    "img": f"https://down-th.img.susercontent.com/file/{item_data["image"]}_tn",
                    "market_place": self.market_place,
                    "url": str(url)
                }
            )

            item.categories.add(self.category)
            item.save()

            stat = {
                "discount": ShopeeFormate.discount(item_data["raw_discount"]),
                "sales": ShopeeFormate.sales(item_data["historical_sold"]),
                "reviews": ShopeeFormate.reviews(item_data["item_rating"]["rating_count"][0]),
                "rating_score": ShopeeFormate.rating_score(item_data["item_rating"]["rating_star"]),
                "price": ShopeeFormate.price(item_data["price"])
            }
            ItemStat.objects.create(item=item, parsing=self.parsing, **stat)

            yield item


class LazadaJSONParser(Parser):
    market_place = MarketPlace.LZ
    default_params = {"ajax": "true"}

    _page_param = "page"

    def page_url_gen(self) -> Generator[str]:
        url = URL(
            netlog=settings.LAZADA_BASE_URL,
            path=(self.category.external_id,),
            query=self.params
        )
        for page in range(settings.LAZADA_PAGE_LIMIT):
            url.query = {**url.query, self._page_param: page}
            yield str(url)

    def page_parse(self, url: str, captcha_wait: int, *args, **kwargs) -> Generator[Item]:
        _iterations = 10  # Will be trying fo get data <10> times within "captcha_wait".

        self.driver.get(url)

        captcha = False
        for _ in (range(_iterations)):
            try:
                json_content = self.driver.find_element(By.TAG_NAME, value="pre")
            except NoSuchElementException:
                captcha = True
                self.driver.implicitly_wait(captcha_wait / _iterations)
            else:
                break
        else:
            raise Exception("Cannot find 'pre' element!")

        if captcha:
            self.parsing.captcha_count = self.parsing.captcha_count + 1

        content = json.loads(json_content.text)

        yield from self.collect_items_gen(content)

    def collect_items_gen(self, content: dict | list) -> Generator[Item]:
        list_items = content["mods"]["listItems"]
        for item_data in list_items:
            item, created = Item.objects.get_or_create(
                item_id=item_data["itemId"],
                defaults={
                    "name": item_data["name"],
                    "img": item_data["image"],
                    "market_place": self.market_place,
                    "url": item_data["itemUrl"]
                }
            )

            item.categories.add(self.category)
            item.save()

            stat = {}
            if "discount" in item_data:
                stat["discount"] = LazadaFormate.discount(item_data["discount"])
            if "itemSoldCntShow" in item_data:
                stat["sales"] = LazadaFormate.sales(item_data["itemSoldCntShow"])
            stat["reviews"] = LazadaFormate.reviews(item_data["review"])
            stat["rating_score"] = LazadaFormate.rating_score(item_data["ratingScore"])
            stat["price"] = LazadaFormate.price(item_data["price"])
            ItemStat.objects.create(item=item, parsing=self.parsing, **stat)

            yield item
