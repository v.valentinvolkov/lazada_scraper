from functools import wraps
from typing import Callable


class FormateException(Exception):
    pass


def formate_method(func: Callable):
    @wraps
    def inner(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except Exception as e:
            raise FormateException(e)

    return inner


class LazadaFormate:

    @staticmethod
    def discount(value: str):
        return int(value.split("%", 1)[0])

    @staticmethod
    def price(value: str):
        return int(float(value))

    @staticmethod
    def original_price(value: str):
        return int(float(value))

    @staticmethod
    def sales(value: str):
        value = value.split(" ", 1)[0]
        if "K" in value:
            value = int(float(value[:-1])*1000)

        return value

    @staticmethod
    def rating_score(value: str):
        return int(float(value) * 10)

    @staticmethod
    def reviews(value: str):
        return int(value or 0)


class ShopeeFormate:

    @staticmethod
    def discount(value: str):
        return value

    @staticmethod
    def price(value: int):
        return int(value/100000)

    @staticmethod
    def original_price(value: str):
        return int(float(value))

    @staticmethod
    def sales(value: int):
        return value

    @staticmethod
    def rating_score(value: float):
        return int(value * 10)

    @staticmethod
    def reviews(value: int):
        return value
