#!/usr/bin/env bash

poetry update
poetry export --without-hashes --with dev -o requirements.txt